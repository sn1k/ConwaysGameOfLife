package com.sniksoft.gameoflife;

import com.sniksoft.gameoflife.R;
import com.sniksoft.gameoflife.SimView;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.Window;
import android.view.WindowManager;

public class GameOfLifeActivity extends Activity
{
	
	private static final int MENU_BASIC_RED = 10;
	private static final int MENU_BASIC_WHITE = 11;
	private static final int MENU_BASIC_BLUE = 12;
	private static final int MENU_BASIC_GREEN = 13;
	
	private static final int MENU_SPEED_MAX = 14;
	private static final int MENU_SPEED_FAST = 15;
	private static final int MENU_SPEED_NORMAL = 16;
	private static final int MENU_SPEED_SLOW = 17;
	
	private static final int MENU_CYCLE_TRAILS = 18;
	private static final int MENU_CYCLE_B = 19;
	private static final int MENU_ETC2 = 20;
	private static final int MENU_ETC3 = 21;
	
	private static final int SUBMENU_GROUP0 = 0;
	private static final int SUBMENU_GROUP1 = 1;
	private static final int SUBMENU_GROUP2 = 1;
	
	private static final int LIFE_UPDATEDELAY_SLOW = 1800; // millis to wait between updates
	private static final int LIFE_UPDATEDELAY_NORMAL = 1000; // millis to wait between updates
	private static final int LIFE_UPDATEDELAY_FAST = 500; // millis to wait between updates
	private static final int LIFE_UPDATEDELAY_EXTREME = 160; // millis to wait between updates
	
	private SimView view = null;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        // set fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.main);
        
        Log.w("[debug]","[snik] activity creating view");
        
        view = (SimView) findViewById(R.id.SimView);
        
        if(view != null);
        
    }
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		super.onCreateOptionsMenu(menu);
	
		//menu.add(android.view.Menu.NONE, MENU1, 0, "menu 1")
		//.setIcon(R.drawable.menu1icon);
		
		SubMenu basicMenu = menu.addSubMenu("Basic Colour");
		SubMenu cycleMenu = menu.addSubMenu("Effects");
		SubMenu speedMenu = menu.addSubMenu("Speed");
		
		basicMenu.add(SUBMENU_GROUP0, MENU_BASIC_RED, 0, "red");
		basicMenu.add(SUBMENU_GROUP0, MENU_BASIC_WHITE, 1, "white");
		basicMenu.add(SUBMENU_GROUP0, MENU_BASIC_BLUE, 2, "blue");
		basicMenu.add(SUBMENU_GROUP0, MENU_BASIC_GREEN, 3, "green");
		
		cycleMenu.add(SUBMENU_GROUP1, MENU_CYCLE_TRAILS, 0, "Trails");
		//cycleMenu.add(SUBMENU_GROUP1, MENU_CYCLE_B, 1, "B");
		
		speedMenu.add(SUBMENU_GROUP2, MENU_SPEED_MAX, 0, "max");
		speedMenu.add(SUBMENU_GROUP2, MENU_SPEED_FAST, 1, "fast");
		speedMenu.add(SUBMENU_GROUP2, MENU_SPEED_NORMAL, 2, "normal");
		speedMenu.add(SUBMENU_GROUP2, MENU_SPEED_SLOW, 3, "slow");
		
		
		return true;
	}
    
    @Override
    /** handle all the menu buttons in submenus */
    public boolean onOptionsItemSelected(MenuItem item)
    {
	    switch (item.getItemId())
	    {
	       case MENU_BASIC_RED:
	    	   //Log.w("[debug]","[snik] activity menu1 selected");
	    	   view.changeColour(240,10,10); break; // red
	    	   //view.setCycling(false, 0);break;
	       case MENU_BASIC_WHITE:
	    	   view.changeColour(255,255,255); break;
	    	   //view.setCycling(false, 0); break;
	       case MENU_BASIC_BLUE:
	    	   view.changeColour(10,10,240); break; // blue
	    	   //view.setCycling(false, 0); break;
	       case MENU_BASIC_GREEN:
	    	   view.changeColour(10,230,10); break; // green
	    	   //view.setCycling(false, 0); break;
	    	   
	       case MENU_CYCLE_TRAILS:
	    	   view.setCycling(true, 0); break;
	       case MENU_CYCLE_B:
	    	   view.setCycling(true, 1); break;
	    	   
	       case MENU_SPEED_MAX:
	    	   view.setUpdateDelay(LIFE_UPDATEDELAY_EXTREME); break;
	       case LIFE_UPDATEDELAY_FAST:
	    	   view.setUpdateDelay(LIFE_UPDATEDELAY_FAST); break;
	       case MENU_SPEED_NORMAL:
	    	   view.setUpdateDelay(LIFE_UPDATEDELAY_NORMAL); break;
	       case MENU_SPEED_SLOW:
	    	   view.setUpdateDelay(LIFE_UPDATEDELAY_SLOW); break;
	    	default:break;
	    }
	    
	    return true;
    }
}




