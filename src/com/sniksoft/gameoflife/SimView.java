package com.sniksoft.gameoflife;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


/** class describes the derived view that the activity displays. Handles create/destroy stuff and key input */
public class SimView extends SurfaceView implements SurfaceHolder.Callback
{
	private SimThread gameThread; // create the thread object that runs the main game
	
	
	/** constructor */
	public SimView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		
		// register our interest in hearing about changes to our surface
        SurfaceHolder holder = getHolder();
        holder.addCallback(this); // get our surfaceX functions to work
        
		Log.w("[debug]","[snik] SimView constructor");
		
		// create the main game thread
		gameThread = new SimThread(holder, context, new Handler() 
		{
            public void handleMessage(Message m) {/* Use for pushing back messages.*/        }
        }); // starts the combat thread
		
		setFocusable(true); // make sure we get key events
		setFocusableInTouchMode(true);
        requestFocus();
	}
	
	@Override
	public void onSizeChanged(int w, int h, int oldW, int oldH)
	{
		if(w==0 || h==0) return; // dodgy call, good one will follow
		
		Log.w("[debug]","[snik] screensize "+w+", "+h);
		
		gameThread.setDimensions(w, h);
	}

	
	
	/** handle touchscreen evensts */
	public boolean onTouchEvent(MotionEvent event)
	{
		//Log.w("[debug]","[snik] onTouchEvent");
		gameThread.onTouch(event);
		return true;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
	{
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder)
	{
		Log.w("[debug]","[snik] surfaceCreated, starting thread");
		gameThread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder)
	{
		// try to close thread until it closes
        boolean retry = true;
        gameThread.kill();
        
        while (retry)
        {
            try
            {
            	gameThread.join(); // block us until game thread is destroyed
                retry = false;
            } catch (InterruptedException e) {}
        }
	}
	
	/** sets the block colour */
	public void changeColour(int r, int g, int b)
	{
		gameThread.setBlockColour(r,g,b);
	}
	
	public void setCycling(boolean on, int type)
	{
		gameThread.setCycling(on, type);
	}
	
	public void setUpdateDelay(int millis)
	{
		gameThread.setUpdateDelay(millis);
	}
	
}
