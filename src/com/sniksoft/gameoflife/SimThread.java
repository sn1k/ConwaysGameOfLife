package com.sniksoft.gameoflife;

//import java.util.Random;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

public class SimThread extends Thread
{
	
	
	
	
	private static final int LIFE_BLOCKSIZE = 10; // dims of block
	private static final int LIFE_GRIDSPACING = 5; // pixels between blocks
	
	private static final int LIFE_NUMBEROF_BUFFERS = 2;
	private static final int LIFE_BUFFER_SIZEX = 32; // pixels between blocks
	private static final int LIFE_BUFFER_SIZEY = 32; // pixels between blocks
	
	private static final int LIFE_RENDER_OFFSETX = 20; // pixels before rendering starts
	private static final int LIFE_RENDER_OFFSETY = 20; // pixels before rendering starts
	
	
	
	// admin
	
	private SurfaceHolder mSurfaceHolder;
	private Context mContext;
	private Handler mHandler;
	
	private long lastUpdate;
		
	private static int screenWidth;
	private static int screenHeight;
	
	private boolean running;
	
	// game stuff
	
	private boolean[][][] buffers; // holds the buffers for state and calculations (they flip backbuffer style)
	
	private int stateBufferIndex; // holds the index of the current state buffer (the other will be the calculation buffer)
	
	//private Random rand;
	// colour stuff
	private Paint blockPaint;
	private int lastColourRed = 255;
	private int lastColourGreen = 255;
	private int lastColourBlue = 255;
	
	private char[][] colourBuffer; // holds the current colour state of each block
	
	private boolean cyclingColours; // if we should not be using the cycling colours (they change themselves)
	private int cyclingType;
	
	// variable update speed
	private int updateDelay = 1000; // set in menu
	
	// ------ fps counting ----
	
	private int fpsCount = 0;
	private int fpsLast = 0;
	private float fpsCurrentTime;
	private Paint textPaint;
	private Paint textBackingPaint;
	
	// ------
	
	/** constructor handles admin for surface and callbacks, sorts all game variables */
	public SimThread(SurfaceHolder surfaceHolder, Context context, Handler handler)
	{
		Log.w("[debug]","[snik] SimThread constructor");
		
		mSurfaceHolder = surfaceHolder;
        mHandler = handler;
        mContext = context;
        
        if(mHandler != null); // gets rid of 'unread' warnings
        if(mContext != null); // gets rid of 'unread' warnings
                
        lastUpdate = System.currentTimeMillis();
        running = true;
        
        //rand = new Random();
        textPaint = new Paint();
        textPaint.setColor(Color.WHITE);
        
        textBackingPaint = new Paint();
        textBackingPaint.setColor(Color.BLACK);
        
        blockPaint = new Paint();
        blockPaint.setColor(Color.WHITE);
        
        init();
	}
	
	
	/** called by the view to let us know the screen dims */
	public void setDimensions(int w, int h)
	{
		screenWidth = w;
        screenHeight = h;
                
        Log.w("[debug]","[snik] screen dimensions "+screenWidth+"x"+screenHeight);
	}
	
	
	/** request thread stops */
	public void kill()
	{
		running = false;
	}
	
	
	public void run()
	{
		Log.w("[debug]","[snik] simthread running"+((running == false)?"flag false":"flag true"));
		
		while(running)
		{			
			Canvas canvas = null;
            try 
            {
            	canvas = mSurfaceHolder.lockCanvas(null);
            	            	
                synchronized (mSurfaceHolder)
                {
                	// update
        			if(System.currentTimeMillis() > lastUpdate + updateDelay) // if time to update
        			{
        				long delta = System.currentTimeMillis() - lastUpdate;//fpsCurrentTime
        				
        				fpsCurrentTime += delta;
        				if(fpsCurrentTime >= 1000)
        				{
        					//Log.w("[debug]","[snik] fps "+fpsCount);
        					fpsLast = fpsCount; // store for rendering
        					fpsCount = 0;
        					fpsCurrentTime = 0;
        				}
        				
        				// UPDATE
        				update(delta);
        				
        				lastUpdate = System.currentTimeMillis(); // set last update time
        			}
        			else // wait 20ms and try again
        			{
        				try { sleep(20); } catch (InterruptedException e) 
        				{	e.printStackTrace();	} 
        			}
        			
        			// RENDER HERE
                    ++fpsCount;
                    
                    renderBuffer(canvas);
                    
                    //drawHud(canvas);
                }
            }
            finally 
            {
                if (canvas != null) {
                    mSurfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
		}
	}
	
	
	// ---------- game ----------------
	
	private void init()
	{		
		buffers = new boolean[LIFE_NUMBEROF_BUFFERS][LIFE_BUFFER_SIZEX][LIFE_BUFFER_SIZEY];
						
		// blinker
		buffers[0][1][0] = true;
		buffers[0][1][1] = true;
		buffers[0][1][2] = true;
		
		// toad
		buffers[0][4][1] = true;
		buffers[0][4][2] = true;
		buffers[0][5][3] = true;
		buffers[0][6][0] = true;
		buffers[0][7][1] = true;
		buffers[0][7][2] = true;
		
		// beacon
		buffers[0][0][5] = true;
		buffers[0][1][5] = true;
		buffers[0][0][6] = true;
		buffers[0][3][7] = true;
		buffers[0][2][8] = true;
		buffers[0][3][8] = true;
				
		stateBufferIndex = 0;
		
		// setup colour buffer
		colourBuffer = new char[LIFE_BUFFER_SIZEX][LIFE_BUFFER_SIZEY];
		
		for(int y=0; y < LIFE_BUFFER_SIZEY; ++y)
			for(int x=0; x < LIFE_BUFFER_SIZEX; ++x)
				colourBuffer[x][y] = 0; 
	}
	
	
	/** does our life rules updating */
	private void update(float delta)
	{
		boolean currentBlockState = false;
		
		// get the next buffer in our array to put results in
		int calcBuffer = (stateBufferIndex+1) % LIFE_NUMBEROF_BUFFERS;
				
		for(int blocky=0; blocky < LIFE_BUFFER_SIZEY; ++blocky)
		{
			for(int blockx=0; blockx < LIFE_BUFFER_SIZEX; ++blockx)
			{
				int startX=0, startY=0, finX=0, finY=0;
				int neighbours =0;
				
				currentBlockState = buffers[stateBufferIndex][blockx][blocky];
								
				// handle edge cases
				if(blockx == 0) startX = 1;
				if(blocky == 0) startY = 1;
				if(blockx == LIFE_BUFFER_SIZEX-1) finX = 1;
				if(blocky == LIFE_BUFFER_SIZEY-1) finY = 1;
				
				for(int y = -1+startY; y <= 1 - finY; ++y)
				{
					for(int x = -1+startX; x <= 1- finX; ++x)
					{
						if(x == 0 && y == 0) continue; // dont count self
						
						// count up neighbours
						if(buffers[stateBufferIndex][blockx+x][blocky+y])
							++neighbours;
					}
				}
				
				// handle colour cycling if it's on
				if(cyclingColours)
				{
					if(colourBuffer[blockx][blocky]>= 160)
					{
						colourBuffer[blockx][blocky] -= 160;
					}
					else
						colourBuffer[blockx][blocky] = 0;
				}
				
				
				// rules				
				if(currentBlockState)
				{
					if(neighbours < 2) // kill starve
						buffers[calcBuffer][blockx][blocky] = false; // Log.w("[debug]","[snik] starve");
					else if(neighbours <= 3) // content
					{
						buffers[calcBuffer][blockx][blocky] = true; // Log.w("[debug]","[snik] content");
						if(cyclingColours)
							colourBuffer[blockx][blocky] = 255; // renew colour
					}
					else if(neighbours > 3) // overcrowd
						buffers[calcBuffer][blockx][blocky] = false; // Log.w("[debug]","[snik] overcrowding");
				}
				else
				{
					if(neighbours == 3) // spawn
					{
						buffers[calcBuffer][blockx][blocky] = true; // Log.w("[debug]","[snik] spawn");
						if(cyclingColours)
							colourBuffer[blockx][blocky] = 255;
					}
					else
						buffers[calcBuffer][blockx][blocky] = false; // Log.w("[debug]","[snik] stays dead");
				}
			}
		}
		
		// update buffer pointer to next
		stateBufferIndex = calcBuffer;
	}
	
	// -------- colour changing ---------
	
	/** allows the external setting of the block colour (0->255 format) */
	public void setBlockColour(int r, int g, int b)
	{
		blockPaint.setARGB(255, r, g, b);
		
		// store incase we're working with effects and need rgb values quickly
		lastColourRed = r;
		lastColourGreen = g;
		lastColourBlue = b;
	}
	
	public void setCycling(boolean cycling, int type)
	{
		cyclingColours = cycling;
		cyclingType = type;
	}
	
	// speed
	
	public void setUpdateDelay(int millis)
	{
		updateDelay = millis;
	}
	
	// ---------- rendering ----------------
	
	
	private void renderBuffer(Canvas canvas)
	{
		canvas.drawRect(0, 0, screenWidth, screenHeight, textBackingPaint);
				
		for(int blocky=0; blocky < LIFE_BUFFER_SIZEY; ++blocky)
		{
			for(int blockx=0; blockx < LIFE_BUFFER_SIZEX; ++blockx)
			{
				// if block is off and we're not cycling (cycling allows trails) skip
				if(!buffers[stateBufferIndex][blockx][blocky] && !cyclingColours)
					continue;
				
				// if off block and cycling, still don't want to draw if colour is black
				if(cyclingColours && colourBuffer[blockx][blocky] == 0)
					continue;
				
				// position
				int xpos = blockx*LIFE_BLOCKSIZE + ((blockx-1)*LIFE_GRIDSPACING) + LIFE_RENDER_OFFSETX;
				int ypos = blocky*LIFE_BLOCKSIZE + ((blocky-1)*LIFE_GRIDSPACING) + LIFE_RENDER_OFFSETY;
				
				// calculate the next colour if they're cycling
				if(cyclingColours)
				{
					int amount = colourBuffer[blockx][blocky];
					blockPaint.setARGB(amount, lastColourRed, lastColourGreen, lastColourBlue);
				}
				
				// draw
				canvas.drawRect(xpos, ypos, xpos+LIFE_BLOCKSIZE, ypos+LIFE_BLOCKSIZE, blockPaint);
			}
		}
	}
	
	/** draws the score and multiplier text onscreen */
	private void drawHud(Canvas canvas)
	{
		// draw hud bar for text backing
		canvas.drawRect(0, 18, screenWidth, 32, textBackingPaint);

        float pos[] = {2,30, 10,30, 18,30};
        canvas.drawPosText(String.valueOf(fpsLast), pos, textPaint);
	}
	
	// ---------- accessors ----------------
	
	public static int getScreenWidth()
	{
		return screenWidth;
	}
	
	public static int getScreenHeight()
	{
		return screenHeight;
	}
	
	
	// ---------- control stuff ----------------
	
	/** sets the block at the given coords to on */
	private void addToBuffer(int x, int y)
	{
		if(x < LIFE_BUFFER_SIZEX && y < LIFE_BUFFER_SIZEY )
			buffers[stateBufferIndex][x][y] = true;
	}
	
	/** touchscreen input */
	public boolean onTouch(MotionEvent event)
	{
		// get touchx/y +set dest
		float x = event.getX();
		float y = event.getY();
		
		
		int xBlock = (int)(x-LIFE_RENDER_OFFSETX)/(LIFE_BLOCKSIZE+LIFE_GRIDSPACING);
		int yBlock = (int)(y-LIFE_RENDER_OFFSETY)/(LIFE_BLOCKSIZE+LIFE_GRIDSPACING);
		
		addToBuffer(xBlock,yBlock);
		
		return true;
	}
	
}
